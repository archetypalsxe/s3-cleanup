# Terraform Instructions

Since we are using Terragrunt, we can dynamically set the AWS provider based on the `terragrunt-config` file where we have a variable `use_localstack`. More notes on this in the sections for running with/without Localstack

## Requirements
* Terragrunt and Terraform are both installed

### Running with LocalStack
* From the root directory run: `docker-compose up --build` to run the LocalStack docker container
* In the `terraform/terragrunt-config.yaml` file, make sure `use_localstack` is set to true

### Running without LocalStack
* AWS credentials/configs are available (recommend using `aws-vault`)
* In the `terraform/terragrunt-config.yaml` file, make sure `use_localstack` is set to false

## Running Terragrunt
All of these commands should be ran from the `terraform` directory

* In order to instantiate terragrunt:
  * `terragrunt init`
* To see the changes that terragrunt will apply:
  * `terragrunt plan`
* To actually apply the terragrunt changes (have the opportunity to approve/deny):
  * `terragrunt apply`
* To destroy all the created resources:
  * `terragrunt destroy`

## Customizing
Configurations can be made by modifying the `terraform/terragrunt-config.yaml` file