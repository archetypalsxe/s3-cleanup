locals {
  config = yamldecode(file("terragrunt-config.yaml"))
  project_name = get_env("PROJECT_NAME", local.config.project_name)
}

inputs = {
  project_name = local.project_name
  aws_region = local.config.aws_region
  working_dir = get_terragrunt_dir()
}

dependencies {
  paths = []
}

terraform {
  extra_arguments "env" {
    commands = ["init", "apply", "refresh", "import", "plan", "taint", "untaint"]
  }

  extra_arguments "init_args" {
    commands = [
      "init"
    ]

    arguments = [
      "--reconfigure",
    ]
  }
}

# Dynamically generating the provider to allow/disallow Localstack
generate "provider" {
  path = "main.tf"
  if_exists = "overwrite"
  contents = <<-EOF
  provider "aws" {
    region  = var.aws_region
    default_tags {
      tags = {
        ProjectName = var.project_name
        ManagedBy = "Terraform"
      }
    }
    %{ if local.config.use_localstack }
    access_key = "test123"
    secret_key = "testabc"
    skip_credentials_validation = true
    skip_requesting_account_id = true
    skip_metadata_api_check = true
    s3_use_path_style = true
    endpoints {
      s3 = "http://localhost:4566"
    }
    %{ endif }
  }

  # Provided by Terragrunt
  terraform {
    backend "s3" {}
  }
  EOF
}

remote_state {
  backend = "s3"

  config = {
    region = local.config.aws_region
    bucket  = "${local.project_name}-remote-state"
    key     = "terraform.tfstate"
    encrypt = true
    dynamodb_table = "${local.project_name}-remote-locks"
  }
}