resource "aws_s3_bucket" "test_bucket" {
  bucket = "${var.project_name}-test-bucket"
  force_destroy = true
}

resource "random_string" "directory_names" {
  count = var.number_of_s3_directories
  length = 15
  special = false
}

resource "aws_s3_object" "sample_file" {
  count = var.number_of_s3_directories
  bucket = aws_s3_bucket.test_bucket.id
  key = "${random_string.directory_names[count.index].id}/index.html"
}

resource "aws_s3_object" "sample_file_2" {
  count = var.number_of_s3_directories
  bucket = aws_s3_bucket.test_bucket.id
  key = "${random_string.directory_names[count.index].id}/font.css"
}

resource "aws_s3_object" "file_in_root_directory" {
  bucket = aws_s3_bucket.test_bucket.id
  key = "randomFile.txt"
}

resource "aws_s3_object" "folder_in_folder" {
  bucket = aws_s3_bucket.test_bucket.id
  key = "${random_string.directory_names[0].id}/innerfolder/file.txt"
}