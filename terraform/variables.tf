variable "aws_region" {
  description = "The deploy target region in AWS"
  type = string
}

variable "project_name" {
  description = "The name of the project to use as a prefix"
  type = string
}

variable "number_of_s3_directories" {
  description = "The number of S3 directories to create in the bucket"
  type = number
  default = 10
}