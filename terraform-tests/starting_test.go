package test

import (
	"testing"
	"os/exec"
	"fmt"
	"strings"
	"github.com/stretchr/testify/assert"
	terraform "github.com/gruntwork-io/terratest/modules/terraform"
)

func TestTerraform(t *testing.T) {
	t.Parallel()
	var project_name string = "s3-cleanup-test"

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../terraform",
		TerraformBinary: "terragrunt",

		// Disable colors in Terraform commands so its easier to parse stdout/stderr
		NoColor:     true,
		Reconfigure: true,
		EnvVars: map[string]string{
			"PROJECT_NAME": project_name,
			"TF_DATA_DIR": ".terraform-" + project_name,
		},
	})

	defer terraform.Destroy(t, terraformOptions)
	terraform.InitAndApply(t, terraformOptions)

	command := exec.Command("../cleanup.py", "-b", "s3-cleanup-test-test-bucket")
	output, error := command.Output()

	// Make sure we didn't get an error
	assert.Equal(t, nil, error)
	// Would be better to actually check the S3 bucket... but starting to run out of time
	// Check to make sure that "Deleting directory" appears 7 times in the output
	assert.Equal(t, 7, strings.Count(string(output), "Deleting directory"))
	fmt.Println(string(output))
}
