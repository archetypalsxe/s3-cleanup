#!/usr/bin/env python3

import boto3, getopt, sys
from datetime import datetime, timezone, timedelta

help_message = f"{sys.argv[0]} -b <s3 bucket name> -k <number of deployments to keep> -d <number of days> -s <number of seconds> -l <use localstack>"

try:
  opts, args = getopt.getopt(sys.argv[1:], "hb:k:d:s:l:", ["help", "bucket=", "keep=", "days=", "seconds=", "localstack="])
except:
  print(help_message)
  sys.exit(2)

# Defaults
bucket_name = 's3-cleanup-test-bucket'
directories_to_keep = 3
days = 0
seconds = 0
aws_url =  "http://localhost.localstack.cloud:4566/"

for opt, arg in opts:
  if opt in ("-h", "--help"):
    print(help_message)
    sys.exit(2)
  elif opt in ("-b", "--bucket"):
    bucket_name = arg
  elif opt in ("-k", "--keep"):
    directories_to_keep = int(arg)
  elif opt in ("-d", "--days"):
    days = int(arg)
  elif opt in ("-s", "--seconds"):
    seconds = int(arg)
  elif opt in ("-l", "--localstack"):
    if arg.lower() in ['false', '0', 'f', 'no']:
      aws_url = False

if aws_url:
  s3_client = boto3.client('s3', endpoint_url=aws_url)
else:
  s3_client = boto3.client('s3')

try:
  s3_client.head_bucket(Bucket=bucket_name)
except:
  print(f"Bucket \"{bucket_name}\" does not seem to exist, or you do not have access")
  sys.exit(2)

unsorted_directories = {}

# If you search with a delimiter of "/", you can get just directories, but then you also don't
# get the creation date and it would require making a second call to AWS for each directory...
for page in s3_client.get_paginator("list_objects_v2").paginate(Bucket=bucket_name):
  for object in page.get('Contents'):

    # If we're not in a directory... ignore it
    if object['Key'].count("/") < 1:
      continue

    directory_name = object['Key'].split("/")[0]
    last_modified = object['LastModified'].timestamp()
    if directory_name not in unsorted_directories:
      unsorted_directories[directory_name] = {'files': [object['Key']], 'last_modified': last_modified}
    else:
      unsorted_directories[directory_name]['files'].append(object['Key'])
      # Probably overkill as you probably wouldn't update a build after another build was created
      if last_modified > unsorted_directories[directory_name]['last_modified']:
        unsorted_directories[directory_name]['last_modified'] = last_modified

# Create a sorted map of directories based on the last modified date
sorted_directory_keys = sorted(
  unsorted_directories,
  key=lambda directory : unsorted_directories[directory]['last_modified'],
  reverse=True
)

if len(sorted_directory_keys) <= directories_to_keep:
  print("There are no directories that need to be deleted")

# Get a UTC timestamp from x days and x seconds ago
directory_age_check = (datetime.now(timezone.utc) - timedelta(days=days) - timedelta(seconds=seconds)).timestamp()

for position in range(directories_to_keep, len(sorted_directory_keys)):
  directory_name = sorted_directory_keys[position]

  if float(unsorted_directories[directory_name]['last_modified']) > directory_age_check:
    continue

  print(f"Deleting directory {directory_name}")

  delete_objects = []
  # Get a list of the files that should be deleted
  for file in unsorted_directories[directory_name]['files']:
    delete_objects.append({'Key': file})

  s3_client.delete_objects(
    Bucket=bucket_name,
    Delete={'Objects': delete_objects},
  )