# S3 Cleanup

## Terraform Instructions
Terraform instructions are available in the README in the `terraform` directory. Terraform is used to setup a testing S3 bucket as well as populate the bucket with some test "deployments"

## Running Terratest Tests
* Make sure that the localstack Docker image is running (directions in Terraform directory)
* From the `terraform-tests`:
  * `./runTests.sh`
* What this tests:
  * Terragrunt runs and deploys the infrastructure within localstack (destroys it at the end of the test)
    * Note: Uses different state S3 bucket/DynamoDB table so fine to run at the same time as the non testing terragrunt
  * Cleanup script runs successfully and deletes 7 directories

## Running the Script
### Pre Requirements
* `python3` and `pip3` are both installed
* Install dependencies:
  * `pip3 install -r requirements.txt`

#### Using Localstack (default)
* Localstack docker container is up and running (documentation in `terraform` directory)

#### Using Live AWS
* AWS credentials/configs are available (recommend using `aws-vault`)
* Use the `-l false` argument when running the script

### Running
* `python3 cleanup.py` or `./cleanup.py`

### Passing in Parameters
* `-b` or `--bucket` can be used to specify a specific S3 bucket to run on
  * Defaults to `s3-cleanup-test-bucket`
* `-k` or `--keep=` can be used to specify the number of deployments that should be kept
  * Defaults to 3
* `-d` or `--days=` can be used to specify the age (in days) of directories that we want to keep
  * Defaults to 0 as in we will delete directories regardless of age
  * If set to 30, will keep deployments that are under 30 days of age, and only delete ones that are older than 30
* `-s` or `--seconds=` can be used to specify the age (in seconds) of directories that we want to keep
  * Similar to the `days` argument, except in seconds, primarily to make testing easier
* `-l` or `--localstack=` can be used to specify that localstack should/shouldn't be used for AWS calls
  * Defaults to using localstack
  * `false`, `f`, `0`, or `no` can be used to turn off localstack